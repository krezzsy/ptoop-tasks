package by.bsuir.ptoop.checksum;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ApplicationStart {
    private final static String FILE_DATA_PATH = "checksum.txt";
    private static ClassLoader classLoader = ApplicationStart.class.getClassLoader();

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        File file = new File(classLoader.getResource(FILE_DATA_PATH).getFile());

        byte[] fileChecksumBytes = Files.readAllBytes(file.toPath());
        String fileChecksum = calculateHashOfFile(fileChecksumBytes);

        System.out.println("Computed checksum: " + fileChecksum);

        Files.write(file.toPath(), "New test checksum string".getBytes());
        byte[] newFileChecksumBytes = Files.readAllBytes(file.toPath());
        String newFileChecksum = calculateHashOfFile(newFileChecksumBytes);

        System.out.println("Computed checksum: " + newFileChecksum);
    }

    private static String calculateHashOfFile(byte[] b) throws NoSuchAlgorithmException {
        byte[] hash = MessageDigest.getInstance("MD5").digest(b);

        return DatatypeConverter.printHexBinary(hash);
    }
}
