package by.bsuir.ptoop.figure.util;

import java.util.HashMap;
import java.util.Map;

import by.bsuir.ptoop.figure.model.figure.FigureName;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;

public class InputFieldResolver {

  private static Map<FigureName, FieldInput> figureDrawServiceMap = new HashMap<>();
  private static CircleFieldInput circleFieldInput = new CircleFieldInput();
  private static SegmentFieldInput segmentFieldInput = new SegmentFieldInput();
  private static RectangleFieldInput rectangleFieldInput = new RectangleFieldInput();
  private static EllipseFieldInput ellipseFieldInput = new EllipseFieldInput();
  private static TriangleFieldInput triangleFieldInput = new TriangleFieldInput();

  static {
    figureDrawServiceMap.put(FigureName.CIRCLE, circleFieldInput);
    figureDrawServiceMap.put(FigureName.SEGMENT, segmentFieldInput);
    figureDrawServiceMap.put(FigureName.RECTANGLE, rectangleFieldInput);
    figureDrawServiceMap.put(FigureName.ELLIPSE, ellipseFieldInput);
    figureDrawServiceMap.put(FigureName.SQUARE, rectangleFieldInput);
    figureDrawServiceMap.put(FigureName.TRIANGLE, triangleFieldInput);
  }

  public void resolveInputFields(FigureName figureName, InputFieldsRequest inputFieldsRequest) {
    figureDrawServiceMap
        .get(figureName)
        .resolve(inputFieldsRequest);
  }
}
