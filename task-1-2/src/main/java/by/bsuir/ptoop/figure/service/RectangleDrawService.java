package by.bsuir.ptoop.figure.service;

import java.util.List;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.FxmlFigure;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class RectangleDrawService implements FigureDrawService {

  private final static Integer RECTANGLE_VERTEX_AMOUNT = 1;

  @Override
  public void draw(FigureDrawRequest request) {
    if (!isFigureDrawRequestValid(request)) {
      throw new IllegalArgumentException("Rectangle figure request invalid");
    }

    List<Point> coordinates = request.getCoordinates();
    FxmlFigure fxmlFigure = request.getFxmlFigure();

    Rectangle rectangle = fxmlFigure.getFigureRectangle();
    rectangle.setX(coordinates.get(0).getXPoint());
    rectangle.setY(coordinates.get(0).getYPoint());
    rectangle.setHeight(request.getHeight());
    rectangle.setWidth(request.getWidth());
    rectangle.setFill(Color.WHITE);
    rectangle.setStroke(Color.BLACK);
    rectangle.setVisible(true);
    System.out.println("Drawed figure: " + rectangle);
  }

  @Override
  public boolean isFigureDrawRequestValid(FigureDrawRequest request) {
    return request != null && request.getFxmlFigure() != null
        && request.getCoordinates() != null
        && request.getCoordinates().size() == RECTANGLE_VERTEX_AMOUNT;
  }
}
