package by.bsuir.ptoop.figure.model.request;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FxmlFigure {

  private Circle figureCircle;
  private Line figureSegment;
  private Polygon figureTriangle;
  private Rectangle figureRectangle;
  private Ellipse figureEllipse;
}
