package by.bsuir.ptoop.figure.model.request;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EllipseInputFields {

  private TextField centerXField;
  private TextField centerYField;
  private Label centerXLabel;
  private Label centerYLabel;
  private TextField radiusXField;
  private TextField radiusYField;
  private Label radiusXLabel;
  private Label radiusYLabel;
}
