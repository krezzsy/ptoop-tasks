package by.bsuir.ptoop.figure.extract;

import java.util.Arrays;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.TriangleInputFields;
import javafx.scene.control.TextField;

public class TriangleDrawRequestExtractor extends FigureDrawRequestExtractor {

  @Override
  public void extract(FigureDrawRequest drawRequest, InputFieldsRequest inputFieldsRequest) {
    TriangleInputFields triangleInputFields = inputFieldsRequest.getTriangleInputFields();
    TextField startPointXField = triangleInputFields.getStartXPoint();
    TextField startPointYField = triangleInputFields.getStartYPoint();
    TextField midPointXField = triangleInputFields.getMidXPoint();
    TextField midPointYField = triangleInputFields.getMidYPoint();
    TextField endPointXField = triangleInputFields.getEndXPoint();
    TextField endPointYField = triangleInputFields.getEndYPoint();

    Double startXTriangle = Double.valueOf(startPointXField.getText());
    Double startYTriangle = Double.valueOf(startPointYField.getText());
    Point startTrianglePoint = new Point(startXTriangle, startYTriangle);

    Double midXTriangle = Double.valueOf(midPointXField.getText());
    Double midYTriangle = Double.valueOf(midPointYField.getText());
    Point midTrianglePoint = new Point(midXTriangle, midYTriangle);

    Double endXTriangle = Double.valueOf(endPointXField.getText());
    Double endYTriangle = Double.valueOf(endPointYField.getText());
    Point endTrianglePoint = new Point(endXTriangle, endYTriangle);

    drawRequest.setCoordinates(Arrays.asList(startTrianglePoint, midTrianglePoint, endTrianglePoint));
  }
}
