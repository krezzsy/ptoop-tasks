package by.bsuir.ptoop.figure.model.figure;

/**
 * Enum used for listing all figures
 */
public enum FigureName {
  CIRCLE("Circle"),
  ELLIPSE("Ellipse"),
  SQUARE("Square"),
  TRIANGLE("Triangle"),
  SEGMENT("Segment"),
  RECTANGLE("Rectangle");

  private String figure;

  FigureName(String figure) {
    this.figure = figure;
  }

  public static FigureName fromValue(String figureName) {
    if (figureName != null) {
      for (FigureName figure : FigureName.values()) {
        if (figure.getFigure().equals(figureName)) {
          return figure;
        }
      }
    }
    return null;
  }

  public String getFigure() {
    return figure;
  }
}
