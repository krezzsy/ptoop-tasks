package by.bsuir.ptoop.figure.service;

import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;

public interface FigureDrawService {

  void draw(FigureDrawRequest request);

  boolean isFigureDrawRequestValid(FigureDrawRequest request);
}
