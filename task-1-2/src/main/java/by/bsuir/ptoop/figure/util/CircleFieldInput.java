package by.bsuir.ptoop.figure.util;

import by.bsuir.ptoop.figure.model.request.CircleInputFields;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CircleFieldInput extends FieldInput {

  @Override
  public void resolve(InputFieldsRequest inputFieldsRequest) {
    CircleInputFields circleInputFields = inputFieldsRequest.getCircleInputFields();

    TextField centerXField = circleInputFields.getCenterXField();
    TextField centerYField = circleInputFields.getCenterYField();
    TextField radiusField = circleInputFields.getRadiusField();
    Label centerYLabel = circleInputFields.getCenterYLabel();
    Label centerXLabel = circleInputFields.getCenterXLabel();
    Label radiusLabel = circleInputFields.getRadiusLabel();

    centerXField.setVisible(true);
    centerYField.setVisible(true);
    radiusField.setVisible(true);
    centerYLabel.setVisible(true);
    centerXLabel.setVisible(true);
    radiusLabel.setVisible(true);

    resolveSubmitButton(inputFieldsRequest);
  }
}
