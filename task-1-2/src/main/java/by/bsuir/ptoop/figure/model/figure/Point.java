package by.bsuir.ptoop.figure.model.figure;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Point {

  private Double xPoint;
  private Double yPoint;
}
