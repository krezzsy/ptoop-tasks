package by.bsuir.ptoop.figure.model.request;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TriangleInputFields {

  private TextField startXPoint;
  private TextField startYPoint;
  private Label startXLabel;
  private Label startYLabel;
  private TextField midXPoint;
  private TextField midYPoint;
  private Label midXLabel;
  private Label midYLabel;
  private TextField endXPoint;
  private TextField endYPoint;
  private Label endXLabel;
  private Label endYLabel;
}
