package by.bsuir.ptoop.figure.extract;

import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;

public abstract class FigureDrawRequestExtractor {

  public abstract void extract(FigureDrawRequest drawRequest, InputFieldsRequest inputFieldsRequest);
}
