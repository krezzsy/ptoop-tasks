package by.bsuir.ptoop.figure.util;

import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.SegmentInputFields;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class SegmentFieldInput extends FieldInput {

  @Override
  public void resolve(InputFieldsRequest inputFieldsRequest) {
    SegmentInputFields segmentInputFields = inputFieldsRequest.getSegmentInputFields();

    TextField startXPoint = segmentInputFields.getStartXPoint();
    TextField startYPoint = segmentInputFields.getStartYPoint();
    TextField endXFieldPoint = segmentInputFields.getEndXFieldPoint();
    TextField endYFieldPoint = segmentInputFields.getEndYFieldPoint();

    Label startYLabel = segmentInputFields.getStartYLabel();
    Label startXLabel = segmentInputFields.getStartXLabel();
    Label endXPointLabel = segmentInputFields.getEndXPointLabel();
    Label endYPointLabel = segmentInputFields.getEndYPointLabel();

    startXPoint.setVisible(true);
    startYPoint.setVisible(true);
    endXFieldPoint.setVisible(true);
    endYFieldPoint.setVisible(true);
    startYLabel.setVisible(true);
    startXLabel.setVisible(true);
    endXPointLabel.setVisible(true);
    endYPointLabel.setVisible(true);

    resolveSubmitButton(inputFieldsRequest);
  }
}
