package by.bsuir.ptoop.figure.extract;

import by.bsuir.ptoop.figure.model.figure.FigureName;

public class FigureDrawRequestExtractorFactory {

  private CircleDrawRequestExtractor circleDrawRequestExtractor = new CircleDrawRequestExtractor();
  private EllipseDrawRequestExtractor ellipseDrawRequestExtractor = new EllipseDrawRequestExtractor();
  private RectangleDrawRequestExtractor rectangleDrawRequestExtractor = new RectangleDrawRequestExtractor();
  private SegmentDrawRequestExtractor segmentDrawRequestExtractor = new SegmentDrawRequestExtractor();
  private SquareDrawRequestExtractor squareDrawRequestExtractor = new SquareDrawRequestExtractor();
  private TriangleDrawRequestExtractor triangleDrawRequestExtractor = new TriangleDrawRequestExtractor();

  public FigureDrawRequestExtractor getFigureDrawRequestExtractor(FigureName figureName) {
    switch (figureName) {
      case CIRCLE:
        return circleDrawRequestExtractor;
      case SEGMENT:
        return segmentDrawRequestExtractor;
      case RECTANGLE:
        return rectangleDrawRequestExtractor;
      case ELLIPSE:
        return ellipseDrawRequestExtractor;
      case TRIANGLE:
        return triangleDrawRequestExtractor;
      case SQUARE:
        return squareDrawRequestExtractor;
    }
    return null;
  }
}
