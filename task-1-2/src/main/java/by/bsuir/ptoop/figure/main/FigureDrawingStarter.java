package by.bsuir.ptoop.figure.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FigureDrawingStarter extends Application {

  private final static String FXML_PATH = "/fxml/draw.fxml";

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource(FXML_PATH));
    Scene scene = new Scene(root, 500, 500);
    primaryStage.setTitle("PTOOP: Figure Drawing: Task");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
