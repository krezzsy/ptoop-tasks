package by.bsuir.ptoop.figure.model.request;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SegmentInputFields {

  private TextField startXPoint;
  private TextField startYPoint;
  private Label startXLabel;
  private Label startYLabel;
  private Label endXPointLabel;
  private Label endYPointLabel;
  private TextField endXFieldPoint;
  private TextField endYFieldPoint;
}
