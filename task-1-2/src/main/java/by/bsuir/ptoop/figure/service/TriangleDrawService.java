package by.bsuir.ptoop.figure.service;

import java.util.List;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.FxmlFigure;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class TriangleDrawService implements FigureDrawService {

  private final static Integer TRIANGLE_VERTEX_AMOUNT = 3;

  public void draw(FigureDrawRequest request) {

    if (!isFigureDrawRequestValid(request)) {
      throw new IllegalArgumentException("Triangle figure request invalid");
    }

    List<Point> coordinates = request.getCoordinates();
    FxmlFigure fxmlFigure = request.getFxmlFigure();
    Polygon triangle = fxmlFigure.getFigureTriangle();

    coordinates.forEach(coordinate -> triangle.getPoints()
        .addAll(coordinate.getXPoint(), coordinate.getYPoint()));

    triangle.setFill(Color.WHITE);
    triangle.setStroke(Color.BLACK);
    triangle.setVisible(true);
    System.out.println("Drawed figure: " + triangle);
  }

  @Override
  public boolean isFigureDrawRequestValid(FigureDrawRequest request) {
    return request != null && request.getFxmlFigure() != null && request.getCoordinates() != null
        && request.getCoordinates().size() == TRIANGLE_VERTEX_AMOUNT;
  }
}
