package by.bsuir.ptoop.figure.util;

import java.util.HashMap;
import java.util.Map;

import by.bsuir.ptoop.figure.model.figure.FigureName;
import by.bsuir.ptoop.figure.service.CircleDrawService;
import by.bsuir.ptoop.figure.service.EllipseDrawService;
import by.bsuir.ptoop.figure.service.FigureDrawService;
import by.bsuir.ptoop.figure.service.RectangleDrawService;
import by.bsuir.ptoop.figure.service.SegmentDrawService;
import by.bsuir.ptoop.figure.service.TriangleDrawService;

public class FigureDrawServiceResolver {

  private static Map<FigureName, FigureDrawService> figureDrawServiceMap = new HashMap<>();
  private static CircleDrawService circleDrawService = new CircleDrawService();
  private static SegmentDrawService segmentDrawService = new SegmentDrawService();
  private static TriangleDrawService triangleDrawService = new TriangleDrawService();
  private static RectangleDrawService rectangleDrawService = new RectangleDrawService();
  private static EllipseDrawService ellipseDrawService = new EllipseDrawService();

  static {
    figureDrawServiceMap.put(FigureName.CIRCLE, circleDrawService);
    figureDrawServiceMap.put(FigureName.SEGMENT, segmentDrawService);
    figureDrawServiceMap.put(FigureName.TRIANGLE, triangleDrawService);
    figureDrawServiceMap.put(FigureName.RECTANGLE, rectangleDrawService);
    figureDrawServiceMap.put(FigureName.SQUARE, rectangleDrawService);
    figureDrawServiceMap.put(FigureName.ELLIPSE, ellipseDrawService);
  }

  public FigureDrawService resolveDrawService(FigureName figureName) {
    return figureDrawServiceMap.get(figureName);
  }
}
