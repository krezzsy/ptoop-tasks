package by.bsuir.ptoop.figure.extract;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.EllipseInputFields;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.control.TextField;

public class EllipseDrawRequestExtractor extends FigureDrawRequestExtractor {

  @Override
  public void extract(FigureDrawRequest drawRequest, InputFieldsRequest inputFieldsRequest) {
    EllipseInputFields ellipseInputFields = inputFieldsRequest.getEllipseInputFields();

    TextField centerXField = ellipseInputFields.getCenterXField();
    TextField centerYField = ellipseInputFields.getCenterYField();
    TextField radiusXField = ellipseInputFields.getRadiusXField();
    TextField radiusYField = ellipseInputFields.getRadiusYField();

    Double centerXEll = Double.valueOf(centerXField.getText());
    Double centerYEll = Double.valueOf(centerYField.getText());
    Point ellipsePoint = new Point(centerXEll, centerYEll);

    Double radiusX = Double.valueOf(radiusXField.getText());
    Double radiusY = Double.valueOf(radiusYField.getText());

    drawRequest.setCenter(ellipsePoint);
    drawRequest.setRadiusX(radiusX);
    drawRequest.setRadiusY(radiusY);
  }
}
