package by.bsuir.ptoop.figure.util;

import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.TriangleInputFields;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class TriangleFieldInput extends FieldInput {

  @Override
  public void resolve(InputFieldsRequest inputFieldsRequest) {
    TriangleInputFields triangleInputFields = inputFieldsRequest.getTriangleInputFields();

    TextField startXPoint = triangleInputFields.getStartXPoint();
    TextField startYPoint = triangleInputFields.getStartYPoint();
    TextField endXPoint = triangleInputFields.getEndXPoint();
    TextField endYPoint = triangleInputFields.getEndYPoint();
    TextField midXPoint = triangleInputFields.getMidXPoint();
    TextField midYPoint = triangleInputFields.getMidYPoint();
    Label startYLabel = triangleInputFields.getStartYLabel();
    Label startXLabel = triangleInputFields.getStartXLabel();
    Label endXLabel = triangleInputFields.getEndXLabel();
    Label endYLabel = triangleInputFields.getEndYLabel();
    Label midXLabel = triangleInputFields.getMidXLabel();
    Label midYLabel = triangleInputFields.getMidYLabel();

    startXPoint.setVisible(true);
    startYPoint.setVisible(true);
    startYLabel.setVisible(true);
    startXLabel.setVisible(true);
    endXLabel.setVisible(true);
    endYLabel.setVisible(true);
    endXPoint.setVisible(true);
    endYPoint.setVisible(true);
    midXLabel.setVisible(true);
    midYLabel.setVisible(true);
    midXPoint.setVisible(true);
    midYPoint.setVisible(true);

    resolveSubmitButton(inputFieldsRequest);
  }
}
