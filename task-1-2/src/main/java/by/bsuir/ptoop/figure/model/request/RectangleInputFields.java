package by.bsuir.ptoop.figure.model.request;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RectangleInputFields {

  private TextField startXPoint;
  private TextField startYPoint;
  private TextField width;
  private TextField height;
  private Label startXLabel;
  private Label startYLabel;
  private Label widthLabel;
  private Label heightLabel;
}
