package by.bsuir.ptoop.figure.controller;

import java.net.URL;
import java.util.ResourceBundle;

import by.bsuir.ptoop.figure.extract.FigureDrawRequestExtractor;
import by.bsuir.ptoop.figure.extract.FigureDrawRequestExtractorFactory;
import by.bsuir.ptoop.figure.model.figure.FigureName;
import by.bsuir.ptoop.figure.model.request.CircleInputFields;
import by.bsuir.ptoop.figure.model.request.EllipseInputFields;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.FxmlFigure;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.RectangleInputFields;
import by.bsuir.ptoop.figure.model.request.SegmentInputFields;
import by.bsuir.ptoop.figure.model.request.TriangleInputFields;
import by.bsuir.ptoop.figure.service.FigureDrawService;
import by.bsuir.ptoop.figure.util.FigureDrawServiceResolver;
import by.bsuir.ptoop.figure.util.InputFieldResolver;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

public class FigureDrawingController implements Initializable {

  @FXML
  private ComboBox<String> figureCombo;
  @FXML
  private TextField centerXField;
  @FXML
  private TextField centerYField;
  @FXML
  private Label centerXLabel;
  @FXML
  private Label centerYLabel;
  @FXML
  private Label startPointXLabel;
  @FXML
  private Label startPointYLabel;
  @FXML
  private Label endPointXLabel;
  @FXML
  private Label endPointYLabel;
  @FXML
  private TextField startPointXField;
  @FXML
  private TextField startPointYField;
  @FXML
  private TextField endPointXField;
  @FXML
  private TextField endPointYField;
  @FXML
  private TextField radiusXField;
  @FXML
  private TextField radiusYField;
  @FXML
  private Label radiusXLabel;
  @FXML
  private Label radiusYLabel;
  @FXML
  private Label radiusLabel;
  @FXML
  private TextField radiusField;
  @FXML
  private Label widthLabel;
  @FXML
  private TextField widthField;
  @FXML
  private Label heightLabel;
  @FXML
  private TextField heightField;
  @FXML
  public Label midPointXLabel;
  @FXML
  public Label midPointYLabel;
  @FXML
  public TextField midPointXField;
  @FXML
  public TextField midPointYField;
  @FXML
  private Button submitButton;
  @FXML
  private Line figureSegment;
  @FXML
  private Circle figureCircle;
  @FXML
  private Polygon figureTriangle;
  @FXML
  private Rectangle figureRectangle;
  @FXML
  private Ellipse figureEllipse;

  private FigureDrawServiceResolver figureDrawServiceResolver = new FigureDrawServiceResolver();
  private FigureDrawRequestExtractorFactory figureDrawRequestExtractorFactory = new FigureDrawRequestExtractorFactory();
  private InputFieldResolver inputFieldResolver = new InputFieldResolver();
  private FigureDrawRequest figureDrawRequest;
  private FigureDrawService figureDrawService;
  private InputFieldsRequest inputFieldsRequest;

  public void initialize(URL location, ResourceBundle resources) {
    initializeFigureListView();

    figureCombo.getSelectionModel().selectedItemProperty().addListener((selected, oldChose, newChose) -> {
      hideAllInputs();

      if (newChose != null) {
        FigureName figureName = FigureName.fromValue(newChose);
        this.figureDrawRequest = new FigureDrawRequest();
        this.figureDrawRequest.setFigureName(figureName);

        this.figureDrawService = figureDrawServiceResolver.resolveDrawService(figureName);
        this.inputFieldsRequest = buildInputFieldsRequest();

        this.inputFieldResolver.resolveInputFields(figureName, inputFieldsRequest);
      }
    });
  }

  @FXML
  protected void handleSubmitButtonAction(ActionEvent event) {
    FxmlFigure fxmlFigure = new FxmlFigure(figureCircle, figureSegment, figureTriangle, figureRectangle, figureEllipse);
    this.figureDrawRequest.setFxmlFigure(fxmlFigure);

    FigureDrawRequestExtractor figureDrawRequestExtractor = figureDrawRequestExtractorFactory
        .getFigureDrawRequestExtractor(this.figureDrawRequest.getFigureName());
    figureDrawRequestExtractor.extract(this.figureDrawRequest, this.inputFieldsRequest);

    this.figureDrawService.draw(figureDrawRequest);
  }

  private void initializeFigureListView() {
    for (FigureName figure : FigureName.values()) {
      figureCombo.getItems().add(figure.getFigure());
    }
  }

  private InputFieldsRequest buildInputFieldsRequest() {
    InputFieldsRequest inputFieldsRequest = new InputFieldsRequest();
    CircleInputFields circleInputFields = new CircleInputFields();
    SegmentInputFields segmentInputFields = new SegmentInputFields();
    RectangleInputFields rectangleInputFields = new RectangleInputFields();
    EllipseInputFields ellipseInputFields = new EllipseInputFields();
    TriangleInputFields triangleInputFields = new TriangleInputFields();

    inputFieldsRequest.setSubmitButton(submitButton);

    circleInputFields.setCenterXField(centerXField);
    circleInputFields.setCenterYField(centerYField);
    circleInputFields.setCenterXLabel(centerXLabel);
    circleInputFields.setCenterYLabel(centerYLabel);
    circleInputFields.setRadiusLabel(radiusLabel);
    circleInputFields.setRadiusField(radiusField);

    segmentInputFields.setStartXLabel(startPointXLabel);
    segmentInputFields.setStartYLabel(startPointYLabel);
    segmentInputFields.setEndXFieldPoint(endPointXField);
    segmentInputFields.setEndYFieldPoint(endPointYField);
    segmentInputFields.setStartXPoint(startPointXField);
    segmentInputFields.setStartYPoint(startPointYField);
    segmentInputFields.setEndXPointLabel(endPointXLabel);
    segmentInputFields.setEndYPointLabel(endPointYLabel);

    rectangleInputFields.setHeight(heightField);
    rectangleInputFields.setHeightLabel(heightLabel);
    rectangleInputFields.setWidth(widthField);
    rectangleInputFields.setWidthLabel(widthLabel);
    rectangleInputFields.setStartXLabel(startPointXLabel);
    rectangleInputFields.setStartYLabel(startPointYLabel);
    rectangleInputFields.setStartXPoint(startPointXField);
    rectangleInputFields.setStartYPoint(startPointYField);

    ellipseInputFields.setCenterXField(centerXField);
    ellipseInputFields.setCenterXLabel(centerXLabel);
    ellipseInputFields.setCenterYField(centerYField);
    ellipseInputFields.setCenterYLabel(centerYLabel);
    ellipseInputFields.setRadiusXField(radiusXField);
    ellipseInputFields.setRadiusYField(radiusYField);
    ellipseInputFields.setRadiusXLabel(radiusXLabel);
    ellipseInputFields.setRadiusYLabel(radiusYLabel);

    triangleInputFields.setStartXLabel(startPointXLabel);
    triangleInputFields.setEndXLabel(endPointXLabel);
    triangleInputFields.setStartYLabel(startPointYLabel);
    triangleInputFields.setStartYPoint(startPointYField);
    triangleInputFields.setStartXPoint(startPointXField);
    triangleInputFields.setMidXLabel(midPointXLabel);
    triangleInputFields.setMidXPoint(midPointXField);
    triangleInputFields.setMidYLabel(midPointYLabel);
    triangleInputFields.setMidYPoint(midPointYField);
    triangleInputFields.setEndXPoint(endPointXField);
    triangleInputFields.setEndYPoint(endPointYField);
    triangleInputFields.setEndYLabel(endPointYLabel);

    inputFieldsRequest.setCircleInputFields(circleInputFields);
    inputFieldsRequest.setSegmentInputFields(segmentInputFields);
    inputFieldsRequest.setRectangleInputFields(rectangleInputFields);
    inputFieldsRequest.setEllipseInputFields(ellipseInputFields);
    inputFieldsRequest.setTriangleInputFields(triangleInputFields);

    return inputFieldsRequest;
  }

  private void hideAllInputs() {
    centerXField.setVisible(false);
    centerXField.clear();
    centerYField.setVisible(false);
    centerYField.clear();
    centerXLabel.setVisible(false);
    centerYLabel.setVisible(false);
    startPointXLabel.setVisible(false);
    startPointYLabel.setVisible(false);
    endPointXLabel.setVisible(false);
    endPointYLabel.setVisible(false);
    startPointXField.setVisible(false);
    startPointXField.clear();
    startPointYField.setVisible(false);
    startPointYField.clear();
    endPointXField.setVisible(false);
    endPointXField.clear();
    endPointYField.setVisible(false);
    endPointYField.clear();
    radiusLabel.setVisible(false);
    radiusField.setVisible(false);
    radiusField.clear();
    submitButton.setVisible(false);
    figureSegment.setVisible(false);
    figureCircle.setVisible(false);
    figureTriangle.setVisible(false);
    figureRectangle.setVisible(false);
    figureEllipse.setVisible(false);
    widthLabel.setVisible(false);
    widthField.setVisible(false);
    widthField.clear();
    heightLabel.setVisible(false);
    heightField.setVisible(false);
    heightField.clear();
    radiusXField.setVisible(false);
    radiusXField.clear();
    radiusXLabel.setVisible(false);
    radiusYField.setVisible(false);
    radiusYField.clear();
    radiusYLabel.setVisible(false);
    midPointXLabel.setVisible(false);
    midPointYLabel.setVisible(false);
    midPointXField.setVisible(false);
    midPointXField.clear();
    midPointYField.setVisible(false);
    midPointYField.clear();
  }
}
