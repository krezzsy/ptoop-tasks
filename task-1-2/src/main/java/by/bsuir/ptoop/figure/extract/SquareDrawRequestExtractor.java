package by.bsuir.ptoop.figure.extract;

import java.util.Collections;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.RectangleInputFields;
import javafx.scene.control.TextField;

public class SquareDrawRequestExtractor extends FigureDrawRequestExtractor {

  @Override
  public void extract(FigureDrawRequest drawRequest, InputFieldsRequest inputFieldsRequest) {
    RectangleInputFields rectangleInputFields = inputFieldsRequest.getRectangleInputFields();
    TextField startPointYField = rectangleInputFields.getStartYPoint();
    TextField startPointXField = rectangleInputFields.getStartXPoint();
    TextField heightField = rectangleInputFields.getHeight();
    TextField widthField = rectangleInputFields.getWidth();

    Double pointX = Double.valueOf(startPointXField.getText());
    Double pointY = Double.valueOf(startPointYField.getText());
    Point rectanglePoint = new Point(pointX, pointY);

    Double width = Double.valueOf(widthField.getText());
    Double height = Double.valueOf(heightField.getText());

    drawRequest.setCoordinates(Collections.singletonList(rectanglePoint));
    drawRequest.setWidth(width);
    drawRequest.setHeight(height);
  }
}
