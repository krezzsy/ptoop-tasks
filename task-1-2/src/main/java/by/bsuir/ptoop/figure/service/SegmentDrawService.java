package by.bsuir.ptoop.figure.service;

import java.util.List;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.FxmlFigure;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.shape.Line;

public class SegmentDrawService implements FigureDrawService {

  private final static Integer SEGMENT_VERTEX_AMOUNT = 2;
  private final static Integer SEGMENT_VERTEX_START = 0;
  private final static Integer SEGMENT_VERTEX_END = 1;

  @Override
  public void draw(FigureDrawRequest figureDrawRequest) {
    if (!isFigureDrawRequestValid(figureDrawRequest)) {
      throw new IllegalArgumentException("Segment should have " + SEGMENT_VERTEX_AMOUNT + " vertex amount");
    }
    List<Point> coordinates = figureDrawRequest.getCoordinates();
    FxmlFigure fxmlFigure = figureDrawRequest.getFxmlFigure();

    Line segment = fxmlFigure.getFigureSegment();
    segment.setStartX(coordinates.get(SEGMENT_VERTEX_START).getXPoint());
    segment.setStartY(coordinates.get(SEGMENT_VERTEX_START).getYPoint());
    segment.setEndX(coordinates.get(SEGMENT_VERTEX_END).getXPoint());
    segment.setEndY(coordinates.get(SEGMENT_VERTEX_END).getYPoint());

    segment.setVisible(true);
    System.out.println("Drawed figure: " + segment);
  }

  @Override
  public boolean isFigureDrawRequestValid(FigureDrawRequest request) {
    return request != null && request.getFxmlFigure() != null
        && request.getCoordinates() != null
        && request.getCoordinates().size() == SEGMENT_VERTEX_AMOUNT;
  }
}
