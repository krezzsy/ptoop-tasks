package by.bsuir.ptoop.figure.util;

import java.util.Optional;

import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.control.Button;

public abstract class FieldInput {

  public abstract void resolve(InputFieldsRequest inputFieldsRequest);

  void resolveSubmitButton(InputFieldsRequest inputFieldsRequest) {
    Button submitButton = inputFieldsRequest.getSubmitButton();
    Optional.ofNullable(submitButton).ifPresent(button -> button.setVisible(true));
  }
}
