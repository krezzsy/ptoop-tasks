package by.bsuir.ptoop.figure.service;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.FxmlFigure;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;

public class EllipseDrawService implements FigureDrawService {

  @Override
  public void draw(FigureDrawRequest request) {
    System.out.println(request);
    if (!isFigureDrawRequestValid(request)) {
      throw new IllegalArgumentException("Ellipse figure request invalid");
    }

    Point center = request.getCenter();
    Double radiusX = request.getRadiusX();
    Double radiusY = request.getRadiusY();

    FxmlFigure fxmlFigure = request.getFxmlFigure();
    Ellipse ellipse = fxmlFigure.getFigureEllipse();

    ellipse.setCenterX(center.getXPoint());
    ellipse.setCenterY(center.getYPoint());
    ellipse.setRadiusX(radiusX);
    ellipse.setRadiusY(radiusY);
    ellipse.setFill(Color.WHITE);
    ellipse.setStroke(Color.BLACK);
    ellipse.setVisible(true);
    System.out.println("Drawed figure: " + ellipse);
  }

  @Override
  public boolean isFigureDrawRequestValid(FigureDrawRequest request) {
    return request != null && request.getFxmlFigure() != null
        && request.getFxmlFigure().getFigureEllipse() != null
        && request.getCenter() != null && request.getCenter().getXPoint() != null
        && request.getCenter().getYPoint() != null
        && request.getRadiusX() != null && request.getRadiusX() > 0
        && request.getRadiusY() != null && request.getRadiusY() > 0;
  }
}
