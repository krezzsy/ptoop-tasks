package by.bsuir.ptoop.figure.model.request;

import java.util.List;

import by.bsuir.ptoop.figure.model.figure.FigureName;
import by.bsuir.ptoop.figure.model.figure.Point;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FigureDrawRequest {

  private FigureName figureName;
  private Point center;
  private Double radius;
  private Double radiusX;
  private Double radiusY;
  private List<Point> coordinates;
  private Double width;
  private Double height;

  private FxmlFigure fxmlFigure;

}
