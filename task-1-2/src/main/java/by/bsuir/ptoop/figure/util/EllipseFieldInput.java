package by.bsuir.ptoop.figure.util;

import by.bsuir.ptoop.figure.model.request.EllipseInputFields;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class EllipseFieldInput extends FieldInput {

  @Override
  public void resolve(InputFieldsRequest inputFieldsRequest) {
    EllipseInputFields ellipseInputFields = inputFieldsRequest.getEllipseInputFields();

    TextField centerXField = ellipseInputFields.getCenterXField();
    TextField centerYField = ellipseInputFields.getCenterYField();
    TextField radiusXField = ellipseInputFields.getRadiusXField();
    TextField radiusYField = ellipseInputFields.getRadiusYField();
    Label centerYLabel = ellipseInputFields.getCenterYLabel();
    Label centerXLabel = ellipseInputFields.getCenterXLabel();
    Label radiusXLabel = ellipseInputFields.getRadiusXLabel();
    Label radiusYLabel = ellipseInputFields.getRadiusYLabel();

    centerXField.setVisible(true);
    centerYField.setVisible(true);
    radiusXField.setVisible(true);
    radiusYField.setVisible(true);
    centerYLabel.setVisible(true);
    centerXLabel.setVisible(true);
    radiusXLabel.setVisible(true);
    radiusYLabel.setVisible(true);

    resolveSubmitButton(inputFieldsRequest);
  }
}
