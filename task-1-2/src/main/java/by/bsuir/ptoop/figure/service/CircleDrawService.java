package by.bsuir.ptoop.figure.service;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.FxmlFigure;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class CircleDrawService implements FigureDrawService {

  @Override
  public void draw(FigureDrawRequest request) {
    if (!isFigureDrawRequestValid(request)) {
      throw new IllegalArgumentException("Circle should have center point and positive radius");
    }
    Point center = request.getCenter();
    Double radius = request.getRadius();
    FxmlFigure fxmlFigure = request.getFxmlFigure();

    Circle circle = fxmlFigure.getFigureCircle();
    circle.setCenterX(center.getXPoint());
    circle.setCenterY(center.getYPoint());
    circle.setRadius(radius);
    circle.setFill(Color.WHITE);
    circle.setStroke(Color.BLACK);

    circle.setVisible(true);
    System.out.println("Drawed figure: " + circle);
  }

  @Override
  public boolean isFigureDrawRequestValid(FigureDrawRequest request) {
    return request != null && request.getFxmlFigure() != null
        && request.getFxmlFigure().getFigureCircle() != null && request.getCenter() != null
        && request.getRadius() != null && request.getRadius() > 0;
  }
}
