package by.bsuir.ptoop.figure.extract;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.CircleInputFields;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import javafx.scene.control.TextField;

public class CircleDrawRequestExtractor extends FigureDrawRequestExtractor {

  @Override
  public void extract(FigureDrawRequest drawRequest, InputFieldsRequest inputFieldsRequest) {
    CircleInputFields circleInputFields = inputFieldsRequest.getCircleInputFields();

    TextField centerXField = circleInputFields.getCenterXField();
    TextField centerYField = circleInputFields.getCenterYField();
    TextField radiusField = circleInputFields.getRadiusField();

    Double centerX = Double.valueOf(centerXField.getText());
    Double centerY = Double.valueOf(centerYField.getText());
    Double radius = Double.valueOf(radiusField.getText());
    Point point = new Point(centerX, centerY);
    drawRequest.setCenter(point);
    drawRequest.setRadius(radius);
  }
}
