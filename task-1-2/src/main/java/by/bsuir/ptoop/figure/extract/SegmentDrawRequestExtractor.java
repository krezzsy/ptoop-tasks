package by.bsuir.ptoop.figure.extract;

import java.util.Arrays;

import by.bsuir.ptoop.figure.model.figure.Point;
import by.bsuir.ptoop.figure.model.request.FigureDrawRequest;
import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.SegmentInputFields;
import javafx.scene.control.TextField;

public class SegmentDrawRequestExtractor extends FigureDrawRequestExtractor {

  @Override
  public void extract(FigureDrawRequest drawRequest, InputFieldsRequest inputFieldsRequest) {
    SegmentInputFields segmentInputFields = inputFieldsRequest.getSegmentInputFields();
    TextField startPointYField = segmentInputFields.getStartYPoint();
    TextField startPointXField = segmentInputFields.getStartXPoint();
    TextField endPointXField = segmentInputFields.getEndXFieldPoint();
    TextField endPointYField = segmentInputFields.getEndYFieldPoint();

    Double startPointX = Double.valueOf(startPointXField.getText());
    Double startPointY = Double.valueOf(startPointYField.getText());
    Point startPoint = new Point(startPointX, startPointY);

    Double endPointX = Double.valueOf(endPointXField.getText());
    Double endPointY = Double.valueOf(endPointYField.getText());
    Point endPoint = new Point(endPointX, endPointY);

    drawRequest.setCoordinates(Arrays.asList(startPoint, endPoint));
  }
}
