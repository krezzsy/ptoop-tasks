package by.bsuir.ptoop.figure.model.request;

import javafx.scene.control.Button;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputFieldsRequest {

  private Button submitButton;
  private CircleInputFields circleInputFields;
  private SegmentInputFields segmentInputFields;
  private RectangleInputFields rectangleInputFields;
  private EllipseInputFields ellipseInputFields;
  private TriangleInputFields triangleInputFields;
}
