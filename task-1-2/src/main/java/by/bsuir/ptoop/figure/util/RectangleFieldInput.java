package by.bsuir.ptoop.figure.util;

import by.bsuir.ptoop.figure.model.request.InputFieldsRequest;
import by.bsuir.ptoop.figure.model.request.RectangleInputFields;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class RectangleFieldInput extends FieldInput {

  @Override
  public void resolve(InputFieldsRequest inputFieldsRequest) {
    RectangleInputFields rectangleInputFields = inputFieldsRequest.getRectangleInputFields();

    TextField startXPoint = rectangleInputFields.getStartXPoint();
    TextField startYPoint = rectangleInputFields.getStartYPoint();
    TextField width = rectangleInputFields.getWidth();
    TextField height = rectangleInputFields.getHeight();
    Label startYLabel = rectangleInputFields.getStartYLabel();
    Label startXLabel = rectangleInputFields.getStartXLabel();
    Label heightLabel = rectangleInputFields.getHeightLabel();
    Label widthLabel = rectangleInputFields.getWidthLabel();

    startXPoint.setVisible(true);
    startYPoint.setVisible(true);
    width.setVisible(true);
    height.setVisible(true);
    startYLabel.setVisible(true);
    startXLabel.setVisible(true);
    heightLabel.setVisible(true);
    widthLabel.setVisible(true);

    resolveSubmitButton(inputFieldsRequest);
  }
}
