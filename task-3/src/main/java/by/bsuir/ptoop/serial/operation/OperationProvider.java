package by.bsuir.ptoop.serial.operation;

import by.bsuir.ptoop.serial.util.UserSelectOption;

import java.util.HashMap;
import java.util.Map;

public class OperationProvider {

    private static Map<UserSelectOption, Operation> optionOperationHashMap = new HashMap<>();
    private static EditObjectOperation editObjectOperation = new EditObjectOperation();
    private static RemoveObjectOperation removeObjectOperation = new RemoveObjectOperation();
    private static AddObjectOperation addObjectOperation = new AddObjectOperation();
    private static SerializeObjectsOperation serializeObjectsOperation = new SerializeObjectsOperation();
    private static DeserializeObjectsOperation deserializeObjectsOperation = new DeserializeObjectsOperation();

    static {
        optionOperationHashMap.put(UserSelectOption.EDIT_OBJECT_OPTION, editObjectOperation);
        optionOperationHashMap.put(UserSelectOption.ADD_OBJECT_OPTION, addObjectOperation);
        optionOperationHashMap.put(UserSelectOption.REMOVE_OBJECT_OPTION, removeObjectOperation);
        optionOperationHashMap.put(UserSelectOption.SERIALIZE_OBJECT_LIST, serializeObjectsOperation);
        optionOperationHashMap.put(UserSelectOption.DESERIALIZE_OBJECT_LIST, deserializeObjectsOperation);
    }

    public void processOperation(UserSelectOption userSelectOption){
        optionOperationHashMap.get(userSelectOption).process();
    }
}
