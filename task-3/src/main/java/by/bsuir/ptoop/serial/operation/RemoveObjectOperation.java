package by.bsuir.ptoop.serial.operation;

import by.bsuir.ptoop.serial.model.Character;

import java.util.List;
import java.util.Objects;

public class RemoveObjectOperation extends Operation {

    @Override
    public void process() {
        System.out.println("You've chosen remove object from list option. Please select object number to remove");

        List<Character> allCharacters = userService.readAllObjects();

        if (allCharacters == null || allCharacters.isEmpty()) {
            System.out.println("Source file doesn't have any objects yet. Nothing to remove");
            return;
        }

        allCharacters.stream().filter(Objects::nonNull).forEach(System.out::println);

        System.out.println("Object number to remove: ");

        Integer objectNumberToRemove = scanner.nextInt();

        userService.removeObject(objectNumberToRemove, allCharacters);

        System.out.println("Object has been removed");
    }
}
