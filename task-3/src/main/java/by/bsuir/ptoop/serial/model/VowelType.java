package by.bsuir.ptoop.serial.model;

public enum VowelType {
  MONOPHTHONG, DIPHTHONG, TRIPHTHONG
}
