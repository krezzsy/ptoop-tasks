package by.bsuir.ptoop.serial.operation;

public class AddObjectOperation extends Operation {

    @Override
    public void process() {
        System.out.println("You've chosen add object from list option.");
        System.out.println("Please, enter new character: ");

        String newCharacterStr = scanner.nextLine();
        userService.addObject(newCharacterStr);
    }
}
