package by.bsuir.ptoop.serial.model;

public class Letter extends Character {
  private final static String LETTER_CASE_FIELD = "letterCase";
  private final static String LETTER_LANG_FIELD = "letterLanguage";
  private final static String LETTER_NUM_FIELD = "letterNumber";

  private String letterCase;
  private String letterLanguage;
  private Integer letterNumber;

  public String getLetterCase() {
    return getString(LETTER_CASE_FIELD);
  }

  public void setLetterCase(String letterCase) {
    put(LETTER_CASE_FIELD, letterCase);
  }

  public String getLetterLanguage() {
    return getString(LETTER_LANG_FIELD);
  }

  public void setLetterLanguage(String letterLanguage) {
    put(LETTER_LANG_FIELD, letterLanguage);
  }

  public Integer getLetterNumber() {
    return getInt(LETTER_NUM_FIELD);
  }

  public void setLetterNumber(Integer letterNumber) {
    put(LETTER_NUM_FIELD, letterNumber);
  }
}
