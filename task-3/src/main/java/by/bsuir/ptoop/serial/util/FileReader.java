package by.bsuir.ptoop.serial.util;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

public class FileReader {

  public List<String> readFile(String path) {
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      File file = new File(classLoader.getResource(path).getFile());
      List<String> lines = Files.readAllLines(file.getAbsoluteFile().toPath(), StandardCharsets.UTF_8);
      return lines;
    } catch (Exception e) {
      throw new RuntimeException("Can't read specified file location", e);
    }
  }
}
