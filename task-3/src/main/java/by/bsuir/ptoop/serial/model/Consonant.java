package by.bsuir.ptoop.serial.model;

public class Consonant extends Letter {

  private final static String CAN_BE_VOWEL_IN_EXCEPTIONAL_CASES = "canBeVowelInExceptionalCases";

  private Boolean canBeVowelInExceptionalCases;

  public Boolean getCanBeVowelInExceptionalCases() {
    return getBoolean(CAN_BE_VOWEL_IN_EXCEPTIONAL_CASES);
  }

  public void setCanBeVowelInExceptionalCases(Boolean canBeVowelInExceptionalCases) {
    put(CAN_BE_VOWEL_IN_EXCEPTIONAL_CASES, canBeVowelInExceptionalCases);
  }
}
