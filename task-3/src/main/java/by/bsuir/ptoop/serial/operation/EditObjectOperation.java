package by.bsuir.ptoop.serial.operation;

import by.bsuir.ptoop.serial.model.Character;

import java.util.List;
import java.util.Objects;

public class EditObjectOperation extends Operation {

    @Override
    public void process() {
        List<Character> characters = userService.readAllObjects();

        if(characters == null || characters.isEmpty()){
            System.out.println("Source file doesn't have any objects yet. Nothing to edit");
            return;
        }

        System.out.println("Please, select object to edit:");

        characters.stream().filter(Objects::nonNull).forEach(System.out::println);

        System.out.println("Object number to edit: ");

        Integer objectNumber = scanner.nextInt();

        if (objectNumber < characters.size() || objectNumber > characters.size()) {
            throw new IllegalArgumentException("Wrong number for object");
        }

        System.out.println("Enter updated character: ");

        String newCharacterString = scanner.nextLine();

        Character newCharacter = userService.editObject(objectNumber, characters, newCharacterString);

        System.out.println("Updated object: " + newCharacter.toString());
    }
}
