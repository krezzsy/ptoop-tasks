package by.bsuir.ptoop.serial.model;

public enum DigitTypeName {
  REAL, INTEGER;
}
