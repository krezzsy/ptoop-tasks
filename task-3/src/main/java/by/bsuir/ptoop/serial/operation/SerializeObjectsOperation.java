package by.bsuir.ptoop.serial.operation;

public class SerializeObjectsOperation extends Operation {

    @Override
    public void process() {
        System.out.println("You've chosen serialize object list");

        userService.serializeObjectList();
    }
}
