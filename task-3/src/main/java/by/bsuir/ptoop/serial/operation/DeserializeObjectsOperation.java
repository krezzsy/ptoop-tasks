package by.bsuir.ptoop.serial.operation;

import java.util.Arrays;
import java.util.List;

public class DeserializeObjectsOperation extends Operation {

    @Override
    public void process() {
        System.out.println("You've chosen deserialize object list");
        System.out.println("Please, enter object list to be deserialized with comma separated format");

        String characterList = scanner.nextLine();
        List<String> charactersStr = Arrays.asList(characterList.split(","));

        userService.deserializeObjectList(charactersStr);
    }
}
