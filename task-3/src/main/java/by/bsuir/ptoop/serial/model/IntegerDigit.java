package by.bsuir.ptoop.serial.model;

public class IntegerDigit extends Digit {

  private final static String ABS_VALUE_FIELD = "absValue";

  private Integer absValue;

  public Integer getAbsValue() {
    return getInt(ABS_VALUE_FIELD);
  }

  public void setAbsValue(Integer absValue) {
    put(ABS_VALUE_FIELD, absValue);
  }
}
