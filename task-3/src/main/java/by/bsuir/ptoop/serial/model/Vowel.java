package by.bsuir.ptoop.serial.model;

public class Vowel extends Letter {

  private final static String VOWEL_TYPE_FIELD = "vowelType";

  private VowelType vowelType;

  public VowelType getVowelType() {
    return (VowelType) get(VOWEL_TYPE_FIELD);
  }

  public void setVowelType(VowelType vowelType) {
    put(VOWEL_TYPE_FIELD, vowelType);
  }
}
