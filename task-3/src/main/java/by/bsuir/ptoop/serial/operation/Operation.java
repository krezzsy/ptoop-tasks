package by.bsuir.ptoop.serial.operation;

import by.bsuir.ptoop.serial.service.UserService;
import by.bsuir.ptoop.serial.service.UserServiceImpl;

import java.util.Scanner;

public abstract class Operation {
    protected UserService userService = new UserServiceImpl();
    protected Scanner scanner = new Scanner(System.in);

    public abstract void process();
}
