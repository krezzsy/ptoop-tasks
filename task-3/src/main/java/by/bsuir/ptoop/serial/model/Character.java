package by.bsuir.ptoop.serial.model;

import org.bson.BasicBSONObject;

public class Character extends BasicBSONObject {

  private final static String FONT_FIELD = "font";
  private final static String VALUE_FIELD = "value";
  private final static String FONT_SIZE_FIELD = "fontSize";

  private String font;
  private String value;
  private Integer fontSize;

  public String getFont() {
    return getString(FONT_FIELD);
  }

  public void setFont(String font) {
    put(FONT_FIELD, font);
  }

  public String getValue() {
    return getString(VALUE_FIELD);
  }

  public void setValue(String value) {
    put(VALUE_FIELD, value);
  }

  public Integer getFontSize() {
    return getInt(FONT_SIZE_FIELD);
  }

  public void setFontSize(Integer fontSize) {
    put(FONT_SIZE_FIELD, fontSize);
  }
}
