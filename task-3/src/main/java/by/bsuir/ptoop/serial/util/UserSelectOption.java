package by.bsuir.ptoop.serial.util;

public enum UserSelectOption {
  EDIT_OBJECT_OPTION(1), ADD_OBJECT_OPTION(2), REMOVE_OBJECT_OPTION(3), SERIALIZE_OBJECT_LIST(
      4), DESERIALIZE_OBJECT_LIST(5);

  Integer option;

  UserSelectOption(Integer option) {
    this.option = option;
  }

  public static UserSelectOption fromValue(Integer option) {
    for (UserSelectOption b : UserSelectOption.values()) {
      if (b.option.equals(option)) {
        return b;
      }
    }
    return null;
  }

  public Integer getValue() {
    return option;
  }

  @Override
  public String toString() {
    return String.valueOf(option);
  }
}
