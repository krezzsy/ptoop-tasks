package by.bsuir.ptoop.serial.service;

import org.bson.BSON;
import org.bson.BSONObject;

import by.bsuir.ptoop.serial.model.Character;

import java.util.List;

public class BsonSerializationService implements SerializationService {

  @Override
  public byte[] serialize(Character character) {
    byte[] serializedBson = BSON.encode(character);

    return serializedBson;
  }

  @Override
  public BSONObject deserialize(byte[] bson) {
    return BSON.decode(bson);
  }
}
