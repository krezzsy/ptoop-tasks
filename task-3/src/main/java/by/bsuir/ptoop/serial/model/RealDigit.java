package by.bsuir.ptoop.serial.model;

public class RealDigit extends Digit {

  private final static String ABS_VALUE_FIELD = "absValue";
  private final static String DECIMAL_ACC_FIELD = "decimalAccuracy";

  private Double absValue;
  private Integer decimalAccuracy;

  public Double getAbsValue() {
    return getDouble(ABS_VALUE_FIELD);
  }

  public void setAbsValue(Double absValue) {
    put(ABS_VALUE_FIELD, absValue);
  }

  public Integer getDecimalAccuracy() {
    return getInt(DECIMAL_ACC_FIELD);
  }

  public void setDecimalAccuracy(Integer decimalAccuracy) {
    put(DECIMAL_ACC_FIELD, decimalAccuracy);
  }
}
