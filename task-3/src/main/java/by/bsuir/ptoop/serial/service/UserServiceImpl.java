package by.bsuir.ptoop.serial.service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import by.bsuir.ptoop.serial.util.FileWriter;
import org.bson.BasicBSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import by.bsuir.ptoop.serial.model.Character;
import by.bsuir.ptoop.serial.util.FileReader;

public class UserServiceImpl implements UserService {

  private final static String FILE_DATA_PATH = "data.txt";

  private final SerializationService serializationService = new BsonSerializationService();
  private final FileReader fileReader = new FileReader();
  private final FileWriter fileWriter = new FileWriter();
  private final Gson gson = new Gson();

  @Override
  public Character editObject(Integer objectNumber, List<Character> oldObjects, String newCharacterString) {
    Character character = gson.fromJson(newCharacterString, Character.class);
    oldObjects.set(objectNumber - 1, character);
    oldObjects.forEach(obj -> fileWriter.writeToFile(FILE_DATA_PATH, obj.toString().getBytes()));
    return character;
  }

  @Override
  public List<Character> readAllObjects() {
    List<Character> characters = Collections.emptyList();

    List<String> objects = fileReader.readFile(FILE_DATA_PATH);
    if (objects != null) {
      characters = objects.stream().filter(Objects::nonNull).map(object -> {
        BasicBSONObject characterBson = (BasicBSONObject) serializationService.deserialize(object.getBytes());
        JsonElement jsonElement = gson.toJsonTree(characterBson.toMap());
        Character character = gson.fromJson(jsonElement, Character.class);

        return character;
      }).collect(Collectors.toList());
    }

    return characters;
  }

  @Override
  public void addObject(String characterString) {
    Character character = gson.fromJson(characterString, Character.class);
    fileWriter.writeToFile(FILE_DATA_PATH, character.toString().getBytes());
  }

  @Override
  public void removeObject(Integer objectToRemove, List<Character> oldCharacters) {
    oldCharacters.remove(objectToRemove);
    oldCharacters.forEach(character -> fileWriter.writeToFile(FILE_DATA_PATH, character.toString().getBytes()));
  }

  @Override
  public void serializeObjectList() {
      List<Character> characters = readAllObjects();

      if(characters == null || characters.isEmpty()){
          System.out.println("Source file doesn't have any objects yet. Nothing to serialize");
          return;
      }
      characters.stream().filter(Objects::isNull).forEach(System.out::println);

      characters.forEach(serializationService::serialize);
  }

  @Override
  public void deserializeObjectList(List<String> characters) {
      characters.forEach(character -> serializationService.deserialize(character.getBytes()));
  }
}
