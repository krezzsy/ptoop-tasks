package by.bsuir.ptoop.serial.util;

import by.bsuir.ptoop.serial.operation.OperationProvider;

import java.util.Scanner;

public class UserMenu {

  private Scanner scanner = new Scanner(System.in);
  private OperationProvider operationProvider = new OperationProvider();

  public void printStartMenu() {
    System.out.println("Please, choose one of the following operations:");
    System.out.println("1. Edit object properties");
    System.out.println("2. Add object to list");
    System.out.println("3. Remove object from list");
    System.out.println("4. Serialize object list");
    System.out.println("5. Deserialize object list");
  }

  public UserSelectOption selectStartOption() {
    System.out.println("Press number of your choice (1-5):");
    Integer choice = null;

    try {
      choice = scanner.nextInt();
    } catch (NumberFormatException e) {
      System.out.println("Invalid option selected. Should be 1-5");
    }
    if (choice != null && choice >= 1 && choice <= 5) {
      return UserSelectOption.fromValue(choice);
    }
    throw new IllegalArgumentException("Invalid option selected. Should be 1-5, but you selected: " + choice);
  }

  public void processUserSelectOption(UserSelectOption option) {
    operationProvider.processOperation(option);
  }
}
