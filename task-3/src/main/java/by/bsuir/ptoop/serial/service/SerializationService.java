package by.bsuir.ptoop.serial.service;

import by.bsuir.ptoop.serial.model.Character;
import org.bson.BSONObject;

public interface SerializationService {

  byte[] serialize(Character character);

  BSONObject deserialize(byte[] bson);
}
