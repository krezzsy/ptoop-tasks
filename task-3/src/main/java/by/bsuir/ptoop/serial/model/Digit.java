package by.bsuir.ptoop.serial.model;

public class Digit extends Character {

  private final static String DIGIT_TYPE = "type";
  private final static String DIGIT_POSITIVE = "isPositive";

  private DigitTypeName type;
  private Boolean isPositive;

  public DigitTypeName getType() {
    return (DigitTypeName) get(DIGIT_TYPE);
  }

  public void setType(DigitTypeName type) {
    put(DIGIT_TYPE, type);
  }

  public Boolean getPositive() {
    return getBoolean(DIGIT_POSITIVE);
  }

  public void setPositive(Boolean positive) {
    put(DIGIT_POSITIVE, positive);
  }
}
