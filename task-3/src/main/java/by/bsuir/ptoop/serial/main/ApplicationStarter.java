package by.bsuir.ptoop.serial.main;

import by.bsuir.ptoop.serial.util.UserMenu;
import by.bsuir.ptoop.serial.util.UserSelectOption;

public class ApplicationStarter {

  private static UserMenu userMenu = new UserMenu();

  public static void main(String[] args) {
    userMenu.printStartMenu();
    UserSelectOption selectedOption = userMenu.selectStartOption();
    userMenu.processUserSelectOption(selectedOption);
  }
}
