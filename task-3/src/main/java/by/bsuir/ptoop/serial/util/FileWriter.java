package by.bsuir.ptoop.serial.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileWriter {

  private final ClassLoader classLoader = getClass().getClassLoader();

  public void writeToFile(String path, byte[] bson) {
    try {
      File file = new File(classLoader.getResource(path).getFile());
      Files.write(file.getAbsoluteFile().toPath(), bson);
    } catch (IOException e) {
      throw new RuntimeException("Can't write to specified file location", e);
    }
  }
}
