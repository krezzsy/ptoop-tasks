package by.bsuir.ptoop.serial.service;

import java.util.List;

import by.bsuir.ptoop.serial.model.Character;

public interface UserService {

  Character editObject(Integer objectNumber, List<Character> oldObjects, String newCharacterString);

  List<Character> readAllObjects();

  void addObject(String character);

  void removeObject(Integer objectNumber, List<Character> oldCharacters);

  void serializeObjectList();

  void deserializeObjectList(List<String> characters);

}
