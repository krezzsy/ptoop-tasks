package by.bsuir.ptoop.serial.main;

import by.bsuir.ptoop.serial.model.CharacterChild;

public class ApplicationStart {

    public static void main(String[] args) {
        CharacterChild character = new CharacterChild();
        character.setFont("Comic");
        character.setFontSize(11);
        character.setValue("X");
        character.setFormat("0101001");

        System.out.println(character);
    }
}
