package by.bsuir.ptoop.serial.model;

public class CharacterChild extends Character {
    private final static String FORMAT_FIELD = "format";

    private String format;

    public String getFormat() {
        return getString(FORMAT_FIELD);
    }

    public void setFormat(String font) {
        put(FORMAT_FIELD, font);
    }
}
